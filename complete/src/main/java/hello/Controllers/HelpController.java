package hello.Controllers;

import hello.Objects.Help;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelpController {

    @RequestMapping("/help")
    public Help help() {
        return new Help();
    }
}