package hello.Controllers;

import java.util.concurrent.atomic.AtomicLong;

import hello.Objects.Ping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/ping")
    public Ping ping(@RequestParam(value="message", defaultValue="Ping Was Successful!") String message) {
        return new Ping(counter.incrementAndGet(), message);
    }
}
