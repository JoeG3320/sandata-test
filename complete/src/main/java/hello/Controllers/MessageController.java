package hello.Controllers;

import hello.Objects.Message;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class MessageController {

    // ID tracking
    private final AtomicLong id = new AtomicLong();

    //In memory storage.
    private HashMap<Long,Message> messages = new HashMap<Long,Message>();


    //Consumes a message Object and stores it in the HashMap
    @RequestMapping("/sendMessage")
    public Message sendMessage(@RequestBody Message message) {
        try{
            //set ID and store the message
            message.setId(id.incrementAndGet());
            storeMessage(message);
            return message;
        }
        catch(Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error when storing the message");
        }
    }

    @RequestMapping("/getMessageById/{id}")
    public Message getMessageById(@PathVariable Long id) {

            Message message = messages.get(id);
            if(message == null){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "A message with the id "+id+ " does not exist");
            }
            else{
                return message;
            }
    }

    // convert hashmap into an array list and return it.
    @RequestMapping("/getAllMessages")
    public ArrayList<Message> getAllMessages() {
        try {
            ArrayList<Message> allMessages = new ArrayList(messages.values());
            return allMessages;
        }
        catch(Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error when retrieving all messages");
        }
    }

    //Store messages in a hash map
    private void storeMessage(Message message){
        messages.put(message.getId(),message);
    }

    //Delete all messages that are currently in memory
    @RequestMapping("/clearMessages")
    public String clearMessages() {
        try {
            messages.clear();
            id.set(0);
            return "All messages were cleared";
        }
        catch(Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error when clearing all messages");
        }

    }

}
