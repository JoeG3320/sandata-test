package hello.Objects;

public class Help {

    private final String help;
    private final String ping;
    private final String sendMessage;
    private final String getMessageById;
    private final String getAllMessages;
    private final String clearAllMessages;

    public Help() {
        this.help = "/help : This command will display information about all other commands";
        this.ping = "/ping : Acts as a test to verify the rest service is up and operational. This can take an optional parameter message";
        this.sendMessage = "/sendMessage : Consumes a post message with the attributes subject, sender, and body any empty attributes will be set to an empty string";
        this.getMessageById = "/getMessageById/{id} : Returns the message with the matching id";
        this.getAllMessages ="/getAllMessages : Returns all messages";
        this.clearAllMessages ="/clearAllMessages : Clears all messages";

    }


    public String getHelp() {
        return help;
    }

    public String getPing() {
        return ping;
    }

    public String getSendMessage() {
        return sendMessage;
    }

    public String getGetMessageById() {
        return getMessageById;
    }

    public String getGetAllMessages() {
        return getAllMessages;
    }

    public String getClearAllMessages() {
        return clearAllMessages;
    }

}
