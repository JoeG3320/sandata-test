package hello.Objects;

public class Message {

    private long id;
    private final String sender;
    private final String subject;
    private final String body;

    public Message(long id, String sender, String subject, String body) {
        this.id = id;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
         this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}
